﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.EventSystems;

public class PlaceObjectOnPlane : MonoBehaviour
{
    [SerializeField]
    ARRaycastManager m_RaycastManager;

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    [SerializeField]
    GameObject m_ObjectToPlace;

    public float RotateSpeed = 50f;
    public float ScaleSpeed = 0.1f;

    private GameObject lastSpawned;

    private bool _rotateLeft;
    private bool _rotateRight;

    private bool _zoomIn;
    private bool _zoomOut;

    private bool placedObject;

    public void OnPressRotateLeft()
    {
        _rotateLeft = true;
    }
    public void OnReleaseLeft()
    {
        _rotateLeft = false;
    }
    public void OnPressRotateRight()
    {
        _rotateRight = true;
    }
    public void OnReleaseRight()
    {
        _rotateRight = false;
    }

    public void OnPressZoomIn()
    {
        _zoomIn = true;
    }
    public void OnReleaseZoomIn()
    {
        _zoomIn = false;
    }
    public void OnPressZoomOut()
    {
        _zoomOut = true;
    }
    public void OnReleaseZoomOut()
    {
        _zoomOut = false;
    }
    public void DestroyObject()
    {
        Destroy(lastSpawned);
        placedObject = false;
    }

    private void Start()
    {
        placedObject = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_rotateLeft)
        {
            lastSpawned.transform.Rotate(Vector3.up, RotateSpeed * Time.deltaTime);
        }
        if (_rotateRight)
        {
            lastSpawned.transform.Rotate(Vector3.down, RotateSpeed * Time.deltaTime);
        }
        if (_zoomIn)
        {
            lastSpawned.transform.localScale += new Vector3(ScaleSpeed, ScaleSpeed, ScaleSpeed);
        }
        if (_zoomOut)
        {
            lastSpawned.transform.localScale -= new Vector3(ScaleSpeed, ScaleSpeed, ScaleSpeed);
        }

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if(touch.phase == TouchPhase.Began && !EventSystem.current.IsPointerOverGameObject())
            {
                if (m_RaycastManager.Raycast(touch.position, s_Hits, TrackableType.PlaneWithinPolygon) && placedObject == false)
                {
                    Pose hitPose = s_Hits[0].pose;

                    lastSpawned = Instantiate(m_ObjectToPlace, hitPose.position, hitPose.rotation);
                    placedObject = true;
                }
            }
        }
    }
}
